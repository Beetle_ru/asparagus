#include <stdio.h>
#include <curl/curl.h>
#include <twitcurl.h>
#include <QApplication>
#include <QPushButton>
//#include <QString>
int twitAutch(std::string userName, std::string passWord, twitCurl &twitterObj) {
    char tmpBuf[1024];
    int tmpVar = 0;
    std::string tmpStr;
    std::string replyMsg;
    /* Set twitter username and password */
    twitterObj.setTwitterUsername( userName );
    twitterObj.setTwitterPassword( passWord );
    
    /* OAuth flow begins */
    /* Step 0: Set OAuth related params. These are got by registering your app at twitter.com */
    twitterObj.getOAuth().setConsumerKey( std::string( "EKufKxQVzsJHt1rjsLAgw" ) );
    twitterObj.getOAuth().setConsumerSecret( std::string( "JuqA1AZsN6KUkLGNtnSbIhLN3NAa71u2mTojHFI" ) );

    /* Step 1: Check if we alredy have OAuth access token from a previous run */
    std::string myOAuthAccessTokenKey("");
    std::string myOAuthAccessTokenSecret("");
    std::ifstream oAuthTokenKeyIn;
    std::ifstream oAuthTokenSecretIn;

    oAuthTokenKeyIn.open( "twitterClient_token_key.txt" );
    oAuthTokenSecretIn.open( "twitterClient_token_secret.txt" );

    memset( tmpBuf, 0, 1024 );
    oAuthTokenKeyIn >> tmpBuf;
    myOAuthAccessTokenKey = tmpBuf;

    memset( tmpBuf, 0, 1024 );
    oAuthTokenSecretIn >> tmpBuf;
    myOAuthAccessTokenSecret = tmpBuf;

    oAuthTokenKeyIn.close();
    oAuthTokenSecretIn.close();

    if( myOAuthAccessTokenKey.size() && myOAuthAccessTokenSecret.size() ) {
        /* If we already have these keys, then no need to go through auth again */
        printf( "\nUsing:\nKey: %s\nSecret: %s\n\n", myOAuthAccessTokenKey.c_str(), myOAuthAccessTokenSecret.c_str() );

        twitterObj.getOAuth().setOAuthTokenKey( myOAuthAccessTokenKey );
        twitterObj.getOAuth().setOAuthTokenSecret( myOAuthAccessTokenSecret );
      
    }
    else {
        /* Step 2: Get request token key and secret */
        twitterObj.oAuthRequestToken( tmpStr );

        /* Step 3: Get PIN  */
        //memset( tmpBuf, 0, 1024 );
        //printf( "\nDo you want to visit twitter.com for PIN (0 for no; 1 for yes): " );
        //gets( tmpBuf );
        //tmpVar = atoi( tmpBuf );

        //if( tmpVar > 0 )
        //{
            /* Ask user to visit twitter.com page and get PIN */
         //   memset( tmpBuf, 0, 1024 );
         //   printf( "\nPlease visit this link in web browser and authorize this application:\n%s", tmpStr.c_str() );
         //   printf( "\nEnter the PIN provided by twitter: " );
          //  gets( tmpBuf );
          //  tmpStr = tmpBuf;
          //  twitterObj.getOAuth().setOAuthPin( tmpStr );
        //}
        //else
        //{
            /* Else, get it via twitcurl PIN handling */
        twitterObj.oAuthHandlePIN( tmpStr );

        if( twitterObj.statusUpdate( tmpStr ) ) {
            twitterObj.getLastWebResponse( replyMsg );
            printf( "\ntwitterClient:: twitCurl::updateStatus web response:\n%s\n", replyMsg.c_str() );
 
            if(strstr(replyMsg.c_str(),"Could not authenticate with OAuth.")) {
                printf( "\nbad login or password\n" );
                return 1;
            }
        }
        else {
            twitterObj.getLastCurlError( replyMsg );
            printf( "\ntwitterClient:: twitCurl::updateStatus error:\n%s\n", replyMsg.c_str() );
        }
            
        //}

        /* Step 4: Exchange request token with access token */
        twitterObj.oAuthAccessToken();

 
        /* Step 5: Now, save this access token key and secret for future use without PIN */
        twitterObj.getOAuth().getOAuthTokenKey( myOAuthAccessTokenKey );
        twitterObj.getOAuth().getOAuthTokenSecret( myOAuthAccessTokenSecret );

        /* Step 6: Save these keys in a file or wherever */
        std::ofstream oAuthTokenKeyOut;
        std::ofstream oAuthTokenSecretOut;

        oAuthTokenKeyOut.open( "twitterClient_token_key.txt" );
        oAuthTokenSecretOut.open( "twitterClient_token_secret.txt" );

        oAuthTokenKeyOut.clear();
        oAuthTokenSecretOut.clear();

        oAuthTokenKeyOut << myOAuthAccessTokenKey.c_str();
        oAuthTokenSecretOut << myOAuthAccessTokenSecret.c_str();

        oAuthTokenKeyOut.close();
        oAuthTokenSecretOut.close();
    }
    /* OAuth flow ends */
    return 0;
}
int twitUserGet(twitCurl &twitterObj, std::string userNik, std::string &userMessages) {
    userMessages = "";
    std::ofstream myOut;

    //tmpStr = "Beetle_ru";

    if( twitterObj.userGet(userNik, false) )
    {
        twitterObj.getLastWebResponse( userMessages );
        printf( "\nUserGet web response:\n%s\n", userMessages.c_str() );
        myOut.open("request.xml");
        myOut.clear();
        myOut << userMessages.c_str();
        myOut.close();
    }
    else
    {
        twitterObj.getLastCurlError( userMessages );
        printf( "\nlineUserGet error:\n%s\n", userMessages.c_str() );
    }
    return 0;
}

int main(int argc, char *argv[]) {
    std::string userName( "Beetle_ru" );
    std::string passWord( "b1beetle" );
    std::string userNik( "Beetle_ru" );
    std::string userMessages( "" );
    twitCurl twitterObj;
    
    QApplication app(argc, argv);
    QPushButton hello("Hello world!");
    hello.resize(100, 30);

    hello.show();

    twitAutch(userName, passWord, twitterObj);
    twitUserGet(twitterObj, userNik, userMessages);

    return app.exec();
}
